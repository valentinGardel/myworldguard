package fr.minecraft.MyWorldGuard;

import Commons.DatabaseConfig;
import org.bukkit.plugin.java.JavaPlugin;

public class Config extends DatabaseConfig
{
	private static Config instance;
	
	public void initConfig(JavaPlugin plugin)
	{
		super.initConfig(plugin);
		Config.instance = this;
	}
	
	public static Config GetConfig()
	{
		return Config.instance;
	}
}
