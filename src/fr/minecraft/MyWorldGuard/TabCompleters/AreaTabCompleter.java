package fr.minecraft.MyWorldGuard.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyWorldGuard.Models.Area;
import fr.minecraft.MyWorldGuard.Utils.Flag;
import fr.minecraft.MyWorldGuard.Utils.Permission;

public class AreaTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res=null;

		switch(args.length)
		{
		case 1:
			res = this.onArg1(args);
			break;
		case 2:
			res = this.onArg2(args, sender);
			break;
		case 3:
			res = this.onArg3(args, sender);
			break;
		case 4:
			res = this.onArg4(args, sender);
			break;
		default:
			break;
		}

		return res;
	}

	private List<String> onArg1(String[] args)
	{
		List<String> res = new ArrayList<String>();

		if("add".startsWith(args[0])) res.add("add");
		if("remove".startsWith(args[0])) res.add("remove");
		if("give".startsWith(args[0])) res.add("give");
		if("create".startsWith(args[0])) res.add("create");
		if("recreate".startsWith(args[0])) res.add("recreate");
		if("extend".startsWith(args[0])) res.add("extend");
		if("setposition".startsWith(args[0])) res.add("setposition");
		if("info".startsWith(args[0])) res.add("info");
		return res;
	}

	private List<String> onArg2(String[] args, CommandSender sender)
	{
		List<String> res = new ArrayList<String>();

		switch(args[0])
		{
		case "setposition":
			if("first".startsWith(args[1])) res.add("first");
			if("second".startsWith(args[1])) res.add("second");
			break;
		case "remove":
			if("area".startsWith(args[1])) res.add("area");
		case "add":
			if("user".startsWith(args[1])) res.add("user");
			if("owner".startsWith(args[1])) res.add("owner");
			if(sender.hasPermission(Permission.FLAG) && "flag".startsWith(args[1])) res.add("flag");
			if(sender.hasPermission(Permission.USE_CHILD) && "child".startsWith(args[1])) res.add("child");
			break;
		case "recreate":
		case "extend":
		case "give":
			if(sender instanceof Player)
			{
				Player player = (Player) sender;
				for(Area area : Area.GetAreas(player.getUniqueId()))
				{
					if(area.name.toUpperCase().startsWith(args[1].toUpperCase()))
						res.add(area.name);
				}
			}
			break;
		case "create":
			if(args[1].isEmpty()) res.add("[<area name>]");
			break;
		default:
			res=null;
			break;
		}

		return res;
	}

	private List<String> onArg3(String[] args, CommandSender sender)
	{
		List<String> res = new ArrayList<String>();

		switch(args[0])
		{
		case "add":
		case "recreate":
		case "remove":
			if(sender instanceof Player)
			{
				Player player = (Player) sender;
				for(Area area : Area.GetAreas(player.getUniqueId()))
				{
					if(area.name.toUpperCase().startsWith(args[2].toUpperCase()))
						res.add(area.name);
				}
			}
			break;
		case "extend":
			if(args[2].isEmpty()) res.add("[<y>]");
			break;
		default:
			res=null;
			break;
		}

		return res;
	}

	private List<String> onArg4(String[] args, CommandSender sender)
	{
		List<String> res = new ArrayList<String>();

		switch(args[0])
		{
		case "remove":
			if(sender.hasPermission(Permission.FLAG) && args[1].equalsIgnoreCase("flag"))
			{
				Area area = Area.GetArea(args[2]);
				if(area!=null)
				{
					for(Flag flag : area.getFlags())
						if(flag.name().toUpperCase().startsWith(args[3].toUpperCase()))
							res.add(flag.name());
				}
			}
			else if (sender.hasPermission(Permission.USE_CHILD) && args[1].equalsIgnoreCase("child")) 
			{
				if(args[3].isEmpty())
					res.add("[child]");
			}
			else
				res = null;
			break;
		case "add":
			if(sender.hasPermission(Permission.FLAG) && args[1].equalsIgnoreCase("flag"))
			{
				Area area = Area.GetArea(args[2]);
				if(area!=null)
				{
					ArrayList<Flag> areaFlags = area.getFlags();
					for(Flag flag : Flag.values())
						if(!areaFlags.contains(flag) && flag.name().toUpperCase().startsWith(args[3].toUpperCase()))
							res.add(flag.name());
				}
			}
			else
				res = null;
			break;
		case "extend":
			if(args[2].isEmpty()) res.add("[<y>]");
			break;
		default:
			res=null;
			break;
		}

		return res;
	}
}
