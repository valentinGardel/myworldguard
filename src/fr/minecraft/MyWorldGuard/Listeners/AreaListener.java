package fr.minecraft.MyWorldGuard.Listeners;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.block.TileState;
import org.bukkit.entity.Animals;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fish;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowman;
import org.bukkit.entity.Squid;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.EntityBlockFormEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.scoreboard.Objective;

import Commons.MinecraftListener;
import fr.minecraft.MyWorldGuard.Models.Area;
import fr.minecraft.MyWorldGuard.Utils.Flag;
import fr.minecraft.MyWorldGuard.Utils.Permission;


public class AreaListener extends MinecraftListener
{
	public static final String FIRST_LINE="[SHOP]";

	public boolean hasFlag(Flag flag, Location l)
	{
		ArrayList<Area> areas = Area.GetAreas(l);
		for(Area area : areas)
		{
			if(area.getFlags().contains(flag))
				return true;
		}
		return false;
	}

	/* valide sur le material est utilisable avec le flag caninteract */
	public boolean isRedstoneInteraction(Material m) {
		Material[] list = {Material.STONE_BUTTON, Material.ACACIA_BUTTON, Material.BIRCH_BUTTON, Material.DARK_OAK_BUTTON, Material.JUNGLE_BUTTON, Material.OAK_BUTTON, Material.SPRUCE_BUTTON, 
				Material.ACACIA_PRESSURE_PLATE, Material.BIRCH_PRESSURE_PLATE, Material.DARK_OAK_PRESSURE_PLATE, Material.HEAVY_WEIGHTED_PRESSURE_PLATE, Material.JUNGLE_PRESSURE_PLATE, 
				Material.LIGHT_WEIGHTED_PRESSURE_PLATE, Material.OAK_PRESSURE_PLATE, Material.SPRUCE_PRESSURE_PLATE, Material.STONE_PRESSURE_PLATE
				, Material.ACACIA_DOOR, Material.BIRCH_DOOR, Material.DARK_OAK_DOOR, Material.IRON_DOOR, Material.JUNGLE_DOOR, Material.OAK_DOOR, Material.SPRUCE_DOOR};
		
		for(Material i : list)
		{
			if(m == i)
				return true;
		}
		return false;
	}
	
	@EventHandler
	public void onSignChangeEvent(SignChangeEvent event)
	{
		if(!event.isCancelled() && !event.getPlayer().hasPermission(Permission.BYPASS))
		{
			if(event.getLine(0).equalsIgnoreCase(FIRST_LINE))
			{
				if(!this.hasFlag(Flag.SHOP, event.getBlock().getLocation()))
				{
					event.getPlayer().sendMessage(ChatColor.RED+"Vous ne pouvez pas cr�er de shop ici!");
					event.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerInteractEntityEvent(PlayerInteractEntityEvent event)
	{
		if(!event.isCancelled() && !event.getPlayer().hasPermission(Permission.BYPASS) && !this.allowEvent(event.getPlayer().getUniqueId(), event.getRightClicked().getLocation())) {
			event.setCancelled(true);
			event.getPlayer().sendMessage(ChatColor.RED+"vous n'avez pas l'autorisation d'interagir ici!");	
		}
	}

	@EventHandler
	public void flagNoDeathCount(PlayerDeathEvent event)
	{
		if(this.hasFlag(Flag.NoDeathCount, event.getEntity().getLocation()))
		{
			Objective obj = Bukkit.getScoreboardManager().getMainScoreboard().getObjective("mort");
			if(obj!=null)
				obj.getScore(event.getEntity().getName()).setScore(Bukkit.getScoreboardManager().getMainScoreboard().getObjective("mort").getScore(event.getEntity().getName()).getScore()-1);
		}
	}

	@EventHandler
	public void flagKeepInventory(PlayerDeathEvent event)
	{
		if(this.hasFlag(Flag.KeepInventory, event.getEntity().getLocation()))
		{
			event.setKeepInventory(true);
			event.setKeepLevel(true);
			event.setDroppedExp(0);
			event.getDrops().clear();
		}
	}

	@EventHandler
	public void flagNoEnderpearl(PlayerTeleportEvent event) {
		if(!event.isCancelled()) {
			if(event.getCause() == TeleportCause.CHORUS_FRUIT || event.getCause() == TeleportCause.ENDER_PEARL) {
				if(this.hasFlag(Flag.NoEnderPearl, event.getTo())) {
					event.setCancelled(true);
					event.getPlayer().sendMessage(ChatColor.RED+"Les enderpearl et chorus-fruit sont interdit ici");
				}
			}
		}
	}

	/**
	 *  interdire le crops jump 
	 *  */
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerCropsJump(PlayerInteractEvent event)
	{
		if(!event.isCancelled())
		{
			// Physical means jump on it
			if (event.getAction() == Action.PHYSICAL)
			{
				Block block = event.getClickedBlock();
				if (block == null) return;
				// If the block is farmland (soil)
				if (block.getType() == Material.FARMLAND) {
					// Deny event and set the block
					if(!this.allowEvent(event.getPlayer().getUniqueId(), event.getClickedBlock().getLocation()))
						event.setCancelled(true);
					//block.setTypeIdAndData(block.getType().getId(), block.getData(), true);
				}
			}
		}
	}

	@EventHandler
	public void breakSpawner(BlockBreakEvent event)
	{
		if(!event.isCancelled() && !event.getPlayer().hasPermission(Permission.BYPASS) && event.getBlock().getType() == Material.SPAWNER && event.getBlock().getWorld().getName().contains("freebuild"))
		{
			event.setCancelled(true);
			event.getPlayer().sendMessage(ChatColor.RED+"vous ne pouvez pas casser les spawner en freebuild, contacter un membre de la mod�ration si vous souhaitez le retirer");	
		}
	}

	@EventHandler
	public void onTntExplode(EntityExplodeEvent event)
	{
		if(event.getEntity().getType() == EntityType.PRIMED_TNT) {
			event.setCancelled(true);
		}
	}

	public boolean isContainer(Block b)
	{
		boolean res = b.getState() instanceof Container || b.getState() instanceof TileState 
				|| b.getType() == Material.ANVIL || b.getType() == Material.CHIPPED_ANVIL || b.getType() == Material.DAMAGED_ANVIL;
		//Bukkit.broadcastMessage("Is container: "+((res)?"true":"false"));
		return res;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void catchChestOpen(PlayerInteractEvent event)
	{
		if(!event.isCancelled() && event.getAction() == Action.RIGHT_CLICK_BLOCK && this.isContainer(event.getClickedBlock()) && !this.allowEvent(event.getPlayer().getUniqueId(), event.getClickedBlock().getLocation()) && !this.hasFlag(Flag.CanOpenChest, event.getClickedBlock().getLocation()) && !event.getPlayer().hasPermission(Permission.BYPASS)){
			event.getPlayer().sendMessage(ChatColor.RED+"Vous ne pouvez pas ouvrir ici");
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onBlockBreakEvent(BlockBreakEvent event)
	{
		if(!event.isCancelled()  && !event.getPlayer().hasPermission(Permission.BYPASS) && !this.allowEvent(event.getPlayer().getUniqueId(), event.getBlock().getLocation())) {
			event.setCancelled(true);
			event.getPlayer().sendMessage(ChatColor.RED+"vous n'avez pas l'autorisation de casser ici!");	
		}
	}

	@EventHandler
	public void onPlayerArmorStandManipulateEvent(PlayerArmorStandManipulateEvent event)
	{
		if(!event.isCancelled() && !event.getPlayer().hasPermission(Permission.BYPASS) && !this.allowEvent(event.getPlayer().getUniqueId(), event.getRightClicked().getLocation())) {
			event.setCancelled(true);
			event.getPlayer().sendMessage(ChatColor.RED+"vous n'avez pas l'autorisation d'utiliser des armor stand ici!");	
		}
	}

	@EventHandler
	public void onPlayerBucketEmptyEvent(PlayerBucketEmptyEvent event) {
		if(!event.isCancelled() && !event.getPlayer().hasPermission(Permission.BYPASS) && !this.allowEvent(event.getPlayer().getUniqueId(), event.getBlock().getLocation())) {
			event.setCancelled(true);
			event.getPlayer().sendMessage(ChatColor.RED+"vous n'avez pas l'autorisation de vider un seau ici!");
		}
	}

	private boolean nearPlayer(Player p, Location l)
	{
		for(Player i : p.getWorld().getPlayers()) {
			if(i != p && i.getLocation().distance(l) < 5)
				return true;
		}
		return false;
	}

	@EventHandler
	public void noLavaOnPlayer(PlayerBucketEmptyEvent event)
	{
		if(!event.isCancelled() && event.getBucket() == Material.LAVA_BUCKET && !event.getPlayer().hasPermission(Permission.USE_LAVA) && this.nearPlayer(event.getPlayer(), event.getBlock().getLocation())) {
			event.setCancelled(true);
			event.getPlayer().sendMessage(ChatColor.RED+"vous ne pouvez pas poser de la lave si pr�s d'un autre joueur!");	
		}
	}

	@EventHandler
	public void onPlayerBucketFillEvent(PlayerBucketFillEvent event)
	{
		if(!event.getPlayer().hasPermission(Permission.BYPASS) && !this.allowEvent(event.getPlayer().getUniqueId(), event.getBlock().getLocation())) {
			event.setCancelled(true);
			event.getPlayer().sendMessage(ChatColor.RED+"vous n'avez pas l'autorisation de remplir un seau ici!");	
		}
	}

	@EventHandler
	public void onBlockPlaceEvent(BlockPlaceEvent event)
	{
		if(!event.isCancelled() && !this.allowEvent(event.getPlayer().getUniqueId(), event.getBlock().getLocation()) && !event.getPlayer().hasPermission(Permission.BYPASS)) {
			event.setCancelled(true);
			event.getPlayer().sendMessage(ChatColor.RED+"vous n'avez pas l'autorisation de placer ici!");	
		}
	}

	/** 
	 * Prevent frostwalker enchant
	 **/
	@EventHandler
	public void onFrostWalkerTrigger(EntityBlockFormEvent event)
	{
		if(!event.isCancelled() && event.getEntity() instanceof Player && event.getBlock().getType() == Material.WATER /*&& event.getNewState().getType() == Material.FROSTED_ICE*/)
		{
			if(!this.allowEvent(event.getEntity().getUniqueId(), event.getBlock().getLocation()))
				event.setCancelled(true);
		}
	}

	@EventHandler
	public void flagInvincibleDamage(EntityDamageEvent event)
	{
		if(!event.isCancelled() && event.getEntity() instanceof Player)
		{
			if(this.hasFlag(Flag.Invincible, event.getEntity().getLocation()))
				event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void flagInvincibleFoodLevel(FoodLevelChangeEvent event)
	{
		if(!event.isCancelled() && event.getEntity() instanceof Player && event.getItem()==null)
		{
			if(this.hasFlag(Flag.Invincible, event.getEntity().getLocation()))
				event.setCancelled(true);
		}
	}

	@EventHandler
	public void flagPvP(EntityDamageByEntityEvent event)
	{
		if(!event.isCancelled() && this.isPlayerHitOtherPlayer(event))
		{
			//if the both players are in the area with flag
			if(!(this.hasFlag(Flag.PvP, event.getDamager().getLocation()) && this.hasFlag(Flag.PvP, event.getEntity().getLocation())))
			{
				event.setCancelled(true);
				if(event.getDamager() instanceof Projectile)
					((Player)((Projectile)event.getDamager()).getShooter()).sendMessage(ChatColor.RED+"pvp d�sactiv�");
				else
					event.getDamager().sendMessage(ChatColor.RED+"pvp d�sactiv�");
			}
		}
	}
	
	/**
	 * Get if a player hit another
	 * @param event event EntityDamageByEntityEvent
	 * @return true if a player hit another, selfdamage return false
	 * */
	private boolean isPlayerHitOtherPlayer(EntityDamageByEntityEvent event)
	{
		//verif entity damaged is a player
		if(event.getEntity() instanceof Player)
		{
			//if damaged by projectile
			if(event.getDamager() instanceof Projectile)
			{
				//if a player shoot the projectile
				if(((Projectile)event.getDamager()).getShooter() instanceof Player)
				{
					return ((Projectile)event.getDamager()).getShooter()!=event.getEntity();
				}
			}
			//if damage directly by a player
			else if(event.getDamager() instanceof Player)
			{
				return event.getEntity()!=event.getDamager();
			}
		}
		return false;
	}

	public boolean cantHitEntity(Entity e)
	{
		return e.getCustomName() != null || e instanceof Villager || e instanceof Squid || e instanceof Fish || e instanceof Animals || e instanceof IronGolem || e instanceof ArmorStand || e instanceof ItemFrame || e instanceof Snowman;
	}

	@EventHandler
	public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		if(!event.isCancelled() && this.cantHitEntity(event.getEntity())) {
			Player player = null;
			if(event.getDamager() instanceof Player)
				player = (Player) event.getDamager();
			else if(event.getDamager() instanceof Projectile && ((Projectile)event.getDamager()).getShooter() instanceof Player)
				player = (Player)((Projectile)event.getDamager()).getShooter();
			if(player != null && !player.hasPermission(Permission.BYPASS) && !this.allowEvent(player.getUniqueId(), event.getEntity().getLocation())) {
				event.setCancelled(true);
				player.sendMessage(ChatColor.RED+"vous n'avez pas l'autorisation d'endommager cette entit�");
			}
		}
	}

	@EventHandler
	public void flagNoMobs(CreatureSpawnEvent event)
	{
		if((!event.isCancelled() && this.hasFlag(Flag.NoMobs, event.getLocation())) && !(event.getSpawnReason() == SpawnReason.SPAWNER_EGG || event.getSpawnReason() == SpawnReason.COMMAND))
		{
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void flagNoMobsGriefing(EntityChangeBlockEvent event)
	{
		
		//Bukkit.broadcastMessage("event fired!");
		if(!event.isCancelled() && this.hasFlag(Flag.NoMobsGriefing, event.getBlock().getLocation())/* && this.allowEvent(event.getEntity().getUniqueId(), event.getBlock().getLocation())*/)
		{
			event.setCancelled(true);
			//Bukkit.broadcastMessage("event canceled!");
		}
	}

	/**
	 * Cancel TNT explosion
	 * */
	@EventHandler
	public void onEntityExplodeEvent(EntityExplodeEvent event)
	{
		//Bukkit.broadcastMessage("event fired!");
		if(!event.isCancelled())
		{
			for(Block block : event.blockList())
				if(Area.GetAreas(block.getLocation()).size()!=0)
				{
					event.setCancelled(true);
					//Bukkit.broadcastMessage("event canceled!");
					return;
				}
		}
	}

	/**
	 * Check if the player has the right at the given location
	 * @param player UUID of the player to check
	 * @param l location to check
	 * @return true if the player has the permission
	 * */
	private boolean allowEvent(UUID player, Location l)
	{
		ArrayList<Area> areas = Area.GetAreas(l);
		//Area master = null;
		boolean res = true;
		
		//on parcourt toute les areas o� on est
		for(Area area : areas)
		{
			//si le joueur n'a pas les droits users
			if(!area.hasUserRights(player))
			{
				//on regarde si l'area qui a detecte qu'il na pas les droits est une parente d'une area o� il se trouve ET ou il a les droits
				res = this.areaIsParentOfAtleastOne(area, areas, player);
				if(!res)
				{
					return false;
				}
			}
		}
		return res;
	}
	
	/**
	 * @param area parent a check
	 * @param areas o� se trouve le joueur
	 * @param player
	 * */
	private boolean areaIsParentOfAtleastOne(Area area, ArrayList<Area> areas, UUID player)
	{
		ArrayList<Integer> childs = area.getChilds();
		for(Area child : areas)
		{
			if(childs.contains(child.getId()) && child.hasUserRights(player))
			{
				return true;
			}
		}
		return false;
	}
}
