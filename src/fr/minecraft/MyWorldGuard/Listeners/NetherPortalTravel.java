package fr.minecraft.MyWorldGuard.Listeners;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.event.world.PortalCreateEvent.CreateReason;

import Commons.MinecraftListener;
import fr.minecraft.MyWorldGuard.Models.Area;


/* listener pour la gestion de creation des portails dans les areas */
public class NetherPortalTravel extends MinecraftListener
{
	public PlayerPortalTravel travel;
	
	public NetherPortalTravel() 
	{
		super();
		this.travel = new PlayerPortalTravel();
	}

	private boolean allowEvent(UUID player, Block b)
	{
		ArrayList<Area> areas = Area.GetAreas(b.getLocation());

		for(Area area : areas)
		{
			if(!area.hasUserRights(player))
				return false;
		}
		return true;
	}

	@EventHandler
	public void onPlayerPortalTeleportEvent(PlayerTeleportEvent event)
	{
		/* si la cause est le tp au portail */
		if(event.getCause() == TeleportCause.UNKNOWN) {
			Travel t = this.travel.getTravel(event.getPlayer());
			/* on verifie que le joueur a un travel et qu il est interdit */
			if(t != null) {
				this.travel.playerTravel.remove(t);
				if(!t.allow) {
					//event.getPlayer().sendMessage(ChatColor.RED+"La teleportation a �t� annul�");
					event.getPlayer().teleport(t.getFrom());
					event.setCancelled(true);
				}
			}
		}
		/* si la cause est la traverse du portail */
		else if(event.getCause() == TeleportCause.NETHER_PORTAL)
			this.travel.addTravel(new Travel(event));
	}


	/* cancel la creation d un portail */
	@EventHandler
	public void onPortalCreateEvent(PortalCreateEvent event)
	{
		if(event.getReason() == CreateReason.NETHER_PAIR)
		{
			if(event.getEntity() instanceof Player) {
				Player player = (Player) event.getEntity();
				for(BlockState i : event.getBlocks())
					if(!this.allowEvent(player.getUniqueId(), i.getBlock())) {
						event.setCancelled(true);
						this.travel.getTravel(player).allow = false;
						player.sendMessage(ChatColor.RED+"Vous ne pouvez pas ouvrir ce portail car la destination est prot�g�");
						return;
					}
			}
			//emepeche une entite non joueur de creer un portail pair
			else {
				event.setCancelled(true);
			}
		}
	}
	
	public class PlayerPortalTravel
	{
		private ArrayList<Travel> playerTravel;

		public PlayerPortalTravel() {
			this.playerTravel = new ArrayList<Travel>();
		}

		/* ajoute un nouveau travel, remplace si le joueur en a deja un */
		public boolean addTravel(Travel travel) {
			Travel t = this.getTravel(travel.player);
			if(t != null)
				this.playerTravel.remove(t);
			return this.playerTravel.add(travel);
		}

		public boolean removeTravel(Player player) {
			return this.playerTravel.remove(this.getTravel(player));
		}

		public Travel getTravel(Player player) {
			for(Travel i : this.playerTravel)
				if(i.getPlayer() == player)
					return i;
			return null;
		}
	}

	public class Travel
	{
		private Player player;
		private Location to, from;
		private boolean allow;

		public Travel(PlayerTeleportEvent event) {
			this.player = event.getPlayer();
			this.allow = true;
			this.from = event.getFrom().clone();
			this.to = event.getTo().clone();
		}

		public Player getPlayer() {
			return player;
		}

		public Location getTo() {
			return to;
		}

		public Location getFrom() {
			return from;
		}

		public boolean isAllow() {
			return allow;
		}
	}
}
