package fr.minecraft.MyWorldGuard.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import Commons.MinecraftListener;
import fr.minecraft.MyWorldGuard.Commands.AreaSelectorCommand;

public class AreaSelectorListener extends MinecraftListener
{
	@EventHandler
	public void guestCantInteract(PlayerInteractEvent event)
	{
		if(event.getItem()!=null && event.getItem().getItemMeta().getDisplayName().equals(AreaSelectorCommand.WAND_NAME))
		{
			event.setCancelled(true);
			if(event.getAction()==Action.LEFT_CLICK_BLOCK)
			{
				event.getPlayer().performCommand("area setposition first");
			}
			else if(event.getAction()==Action.RIGHT_CLICK_BLOCK)
			{
				event.getPlayer().performCommand("area setposition second");
			}
		}
	}
}
