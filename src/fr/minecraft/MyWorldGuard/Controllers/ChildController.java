package fr.minecraft.MyWorldGuard.Controllers;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.minecraft.MyWorldGuard.Models.Area;
import fr.minecraft.MyWorldGuard.Utils.Code;
import fr.minecraft.MyWorldGuard.Utils.Permission;

public class ChildController {

	public static void addChild(CommandSender sender, String parentAreaName, String childAreaName)
	{
		Area parent = Area.GetArea(parentAreaName);

		if(parent != null)
		{
			Area child = Area.GetArea(childAreaName);
			if(child != null)
			{
				if(ChildController.CanUpdateChild(sender, parent))
				{
					if(!parent.getChilds().contains(child.getId()))
					{
						Code code = parent.addChild(child);
						switch(code)
						{
						case Success:
							sender.sendMessage(ChatColor.GREEN+"L'area "+childAreaName+" est maintenant un child de l'area "+parentAreaName);
							break;
						default:
							sender.sendMessage(ChatColor.RED+"Failed");
							break;
						}
					}
					else
						sender.sendMessage(ChatColor.RED+"L'area "+parentAreaName+" a d�j� pour child l'area "+childAreaName+"!");
				}
				else
					sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission de faire ca!");
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+childAreaName+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"L'area "+parentAreaName+" n'existe pas!");
	}

	private static boolean CanUpdateChild(CommandSender sender, Area parent)
	{
		return sender.hasPermission(Permission.USE_CHILD) && (sender.hasPermission(Permission.BYPASS) || (sender instanceof Player && parent.hasOwnerRights(((Player)sender).getUniqueId()))); 
	}

	public static void removeChild(CommandSender sender, String parentAreaName, String childAreaName)
	{
		Area parent = Area.GetArea(parentAreaName);

		if(parent != null)
		{
			Area child = Area.GetArea(childAreaName);
			if(child != null)
			{
				if(ChildController.CanUpdateChild(sender, parent))
				{
					if(parent.getChilds().contains(child.getId()))
					{
						Code code = parent.removeChild(child);
						switch(code)
						{
						case Success:
							sender.sendMessage(ChatColor.GREEN+"L'area "+childAreaName+" est maintenant un child de l'area "+parentAreaName);
							break;
						default:
							sender.sendMessage(ChatColor.RED+"Failed");
							break;
						}
					}
					else
						sender.sendMessage(ChatColor.RED+"L'area "+parentAreaName+" n'a pas pour child l'area "+childAreaName+"!");
				}
				else
					sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission de faire ca!");
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+childAreaName+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"L'area "+parentAreaName+" n'existe pas!");
	}
}
