package fr.minecraft.MyWorldGuard.Controllers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.minecraft.MyWorldGuard.Models.Area;
import fr.minecraft.MyWorldGuard.Utils.Permission;

public class UserController {

	public static void AddUser(CommandSender sender, String areaName, String user)
	{
		@SuppressWarnings("deprecation")
		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(user);

		if(offlinePlayer != null && offlinePlayer.hasPlayedBefore())
		{
			Area area = Area.GetArea(areaName);
			if(area != null)
			{
				if(UserController.CanAddUser(sender, offlinePlayer, area))
				{
					area.addUser(offlinePlayer.getUniqueId());
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+areaName+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Le joueur "+user+" est introuvable");
	}

	public static void RemoveUser(CommandSender sender, String areaName, String user)
	{
		@SuppressWarnings("deprecation")
		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(user);

		if(offlinePlayer != null && offlinePlayer.hasPlayedBefore())
		{
			Area area = Area.GetArea(areaName);
			if(area != null)
			{
				if(UserController.CanRemoveUser(sender, offlinePlayer, area))
				{
					area.removeUser(offlinePlayer.getUniqueId());
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+areaName+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Le joueur "+user+" est introuvable");
	}

	private static boolean CanRemoveUser(CommandSender sender, OfflinePlayer offlinePlayer, Area area)
	{
		//check if the player is a user
		if(!area.getUsers().contains(offlinePlayer.getUniqueId()))
		{
			sender.sendMessage(ChatColor.RED+"Le joueur "+offlinePlayer.getName()+" n'a pas acc�s � cette area!");
			return false;
		}
		//check if the sender can add
		if(sender.hasPermission(Permission.BYPASS))
			return true;
		else if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(area.creator.equals(player.getUniqueId()) || area.getOwners().contains(player.getUniqueId()))
				return true;
			else
			{
				sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission de supprimer un user dans cette area!");
				return false;
			}
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Permission denied");
			return false;
		}
	}

	private static boolean CanAddUser(CommandSender sender, OfflinePlayer offlinePlayer, Area area)
	{
		//check if the player can be add
		if(area.creator.equals(offlinePlayer.getUniqueId()) || area.getOwners().contains(offlinePlayer.getUniqueId())|| area.getUsers().contains(offlinePlayer.getUniqueId()))
		{
			sender.sendMessage(ChatColor.RED+"Le joueur "+offlinePlayer.getName()+" � d�j� acc�s � cette area!");
			return false;
		}
		//check if the sender can add
		if(sender.hasPermission(Permission.BYPASS))
			return true;
		else if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(area.creator.equals(player.getUniqueId()) || area.getOwners().contains(player.getUniqueId()))
				return true;
			else
			{
				sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission d'ajouter un user dans cette area!");
				return false;
			}
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Permission denied");
			return false;
		}
	}

}
