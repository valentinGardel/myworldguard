package fr.minecraft.MyWorldGuard.Controllers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.minecraft.MyWorldGuard.Models.Area;
import fr.minecraft.MyWorldGuard.Utils.Code;
import fr.minecraft.MyWorldGuard.Utils.Permission;

public class OwnerController {

	public static void addOwner(CommandSender sender, String areaName, String owner)
	{
		@SuppressWarnings("deprecation")
		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(owner);

		if(offlinePlayer != null)
		{
			Area area = Area.GetArea(areaName);
			if(area != null)
			{
				if(OwnerController.CanAddOwner(sender, offlinePlayer, area))
				{
					Code code = area.addOwner(offlinePlayer.getUniqueId());
					switch(code)
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Le joueur "+owner+" est maintenant un owner de l'area "+areaName);
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Failed");
						break;
					}
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+areaName+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Le joueur "+owner+" est introuvable");
	}

	public static void removeOwner(CommandSender sender, String areaName, String owner) {
		@SuppressWarnings("deprecation")
		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(owner);

		if(offlinePlayer != null)
		{
			Area area = Area.GetArea(areaName);
			if(area != null)
			{
				if(OwnerController.CanRemoveOwner(sender, offlinePlayer, area))
				{
					Code code =	area.removeOwner(offlinePlayer.getUniqueId());
					switch(code)
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Le joueur "+owner+" n'est plus un owner de l'area "+areaName);
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Failed");
						break;
					}
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+areaName+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Le joueur "+owner+" est introuvable");
	}

	private static boolean CanRemoveOwner(CommandSender sender, OfflinePlayer offlinePlayer, Area area)
	{
		//check if the player is a user
		if(!area.getOwners().contains(offlinePlayer.getUniqueId()))
		{
			sender.sendMessage(ChatColor.RED+"Le joueur "+offlinePlayer.getName()+" n'est pas owner de cette area!");
			return false;
		}
		//check if the sender can add
		if(sender.hasPermission(Permission.BYPASS))
			return true;
		else if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(area.creator.equals(player.getUniqueId()))
				return true;
			else
			{
				sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission de supprimer un owner dans cette area!");
				return false;
			}
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Permission denied");
			return false;
		}
	}

	private static boolean CanAddOwner(CommandSender sender, OfflinePlayer offlinePlayer, Area area)
	{
		//check if the player can be add
		if(area.hasOwnerRights(offlinePlayer.getUniqueId()))
		{
			sender.sendMessage(ChatColor.RED+"Le joueur "+offlinePlayer.getName()+" � d�j� acc�s � cette area!");
			return false;
		}
		else if(area.getUsers().contains(offlinePlayer.getUniqueId()))
		{
			//TODO supprimer auto le user pour pouvoir ajouter le owner
			sender.sendMessage(ChatColor.RED+"Le joueur "+offlinePlayer.getName()+" est d�j� user dans cette area, enlevez-le pour pouvoir l'ajouter en owner!");
			return false;
		}
		//check if the sender can add
		if(sender.hasPermission(Permission.BYPASS))
			return true;
		else if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(area.creator.equals(player.getUniqueId()))
				return true;
			else
			{
				sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission d'ajouter un owner dans cette area!");
				return false;
			}
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Permission denied");
			return false;
		}
	}

}
