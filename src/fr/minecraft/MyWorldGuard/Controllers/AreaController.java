package fr.minecraft.MyWorldGuard.Controllers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.minecraft.MyWorldGuard.Models.Area;
import fr.minecraft.MyWorldGuard.Utils.AreaSelection;
import fr.minecraft.MyWorldGuard.Utils.Code;
import fr.minecraft.MyWorldGuard.Utils.Permission;

public class AreaController {

	public static void create(CommandSender sender, String areaName, AreaSelection selection)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(Area.GetArea(areaName)==null)
			{
				if(selection!=null && selection.isValid())
				{
					//TODO ajouter un nombre d'area max
					Area area = new Area(areaName, player.getUniqueId(), selection.first, selection.second);
					Code code = area.save();
					switch(code)
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"L'area "+areaName+" a �t� cr��!");
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Failed");
						break;
					}
				}
				else
					sender.sendMessage(ChatColor.RED+"La selection de l'area n'est pas valide!");
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+areaName+" existe d�j�!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Cette commande ne peut etre utilis� que par un joueur");
	}

	public static void delete(CommandSender sender, String areaName)
	{
		Area area = Area.GetArea(areaName);
		if(area != null)
		{
			if(AreaController.CanDelete(sender, area))
			{
				Code code = area.delete();
				switch(code)
				{
				case Success:
					sender.sendMessage(ChatColor.GREEN+"L'area "+areaName+" a �t� supprim�!");
					break;
				default:
					sender.sendMessage(ChatColor.RED+"Failed");
					break;
				}
			}
		}
		else
			sender.sendMessage(ChatColor.RED+"L'area "+areaName+" n'existe pas!");
	}

	private static boolean CanDelete(CommandSender sender, Area area) {
		if(sender.hasPermission(Permission.BYPASS))
			return true;
		else if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(area.creator.equals(player.getUniqueId()))
				return true;
			else
			{
				sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission de supprimer cette area!");
				return false;
			}
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Permission denied");
			return false;
		}
	}

	public static void recreate(CommandSender sender, String areaName, AreaSelection selection)
	{
		if(sender instanceof Player)
		{
			Area area = Area.GetArea(areaName);
			if(area!=null)
			{
				if(AreaController.CanRecreate(sender, area))
				{
					if(selection!=null && selection.isValid())
					{
						area.firstLocation=selection.first;
						area.secondLocation=selection.second;
						Code code = area.save();
						switch(code)
						{
						case Success:
							sender.sendMessage(ChatColor.GREEN+"L'area "+areaName+" a �t� recr��!");
							break;
						default:
							sender.sendMessage(ChatColor.RED+"Failed");
							break;
						}
					}
					else
						sender.sendMessage(ChatColor.RED+"La selection de l'area n'est pas valide!");
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+areaName+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Cette commande ne peut etre utilis� que par un joueur");
	}

	private static boolean CanRecreate(CommandSender sender, Area area)
	{
		if(sender.hasPermission(Permission.BYPASS))
			return true;
		else if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(area.creator.equals(player.getUniqueId()))
				return true;
			else
			{
				sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission de recreate cette area!");
				return false;
			}
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Permission denied");
			return false;
		}
	}

	public static void extend(CommandSender sender, String areaName, int y1, int y2)
	{
		Area area = Area.GetArea(areaName);
		if(area!=null)
		{
			if(AreaController.CanExtend(sender, area))
			{
				area.firstLocation.setY(y1);
				area.secondLocation.setY(y2);
				Code code = area.save();
				switch(code)
				{
				case Success:
					sender.sendMessage(ChatColor.GREEN+"L'area "+areaName+" a �t� extend!");
					break;
				default:
					sender.sendMessage(ChatColor.RED+"Failed");
					break;
				}
			}
		}
		else
			sender.sendMessage(ChatColor.RED+"L'area "+areaName+" n'existe pas!");
	}

	private static boolean CanExtend(CommandSender sender, Area area) {
		if(sender.hasPermission(Permission.BYPASS))
			return true;
		else if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(area.creator.equals(player.getUniqueId()))
				return true;
			else
			{
				sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission d'extend cette area!");
				return false;
			}
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Permission denied");
			return false;
		}
	}


	public static void Give(CommandSender sender, String areaName, String creator)
	{
		@SuppressWarnings("deprecation")
		OfflinePlayer player = Bukkit.getOfflinePlayer(creator);
		if(player != null)
		{
			Area area = Area.GetArea(areaName);
			if(area != null)
			{
				if(AreaController.CanGive(sender, area, player))
				{
					area.creator=player.getUniqueId();
					Code code = area.save();
					switch(code)
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"L'area "+areaName+" a �t� donn� � "+creator);
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Failed");
						break;
					}
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+areaName+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Le joueur "+creator+" n'existe pas!");
	}

	private static boolean CanGive(CommandSender sender, Area area, OfflinePlayer offlinePlayer) {
		if(area.creator.equals(offlinePlayer.getUniqueId()))
		{
			sender.sendMessage(ChatColor.RED+"Le joueur "+offlinePlayer.getName()+" est d�j� creator de cette area!");
			return false;
		}
		else if(sender.hasPermission(Permission.BYPASS))
			return true;
		else if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(area.creator.equals(player.getUniqueId()))
				return true;
			else
			{
				sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission de donner cette area!");
				return false;
			}
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Permission denied");
			return false;
		}
	}
}
