package fr.minecraft.MyWorldGuard.Controllers;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import fr.minecraft.MyWorldGuard.Models.Area;
import fr.minecraft.MyWorldGuard.Utils.Code;
import fr.minecraft.MyWorldGuard.Utils.Flag;
import fr.minecraft.MyWorldGuard.Utils.Permission;

public class FlagController {

	public static void addFlag(CommandSender sender, String areaName, String flagName)
	{
		Flag flag = Flag.valueOf(flagName);

		if(flag != null)
		{
			Area area = Area.GetArea(areaName);
			if(area != null)
			{
				if(FlagController.CanUseFlag(sender, flag, area))
				{
					Code code = area.addFlag(flag);
					switch(code)
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Le flag "+flagName+" a �t� ajout� � l'area "+areaName);
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Failed");
						break;
					}
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+areaName+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Le flag "+flagName+" n'existe pas");
	}

	private static boolean CanUseFlag(CommandSender sender, Flag flag, Area area) {
		return sender.hasPermission(Permission.FLAG);
	}

	public static void removeFlag(CommandSender sender, String areaName, String flagName)
	{
		Flag flag = Flag.valueOf(flagName);

		if(flag != null)
		{
			Area area = Area.GetArea(areaName);
			if(area != null)
			{
				if(FlagController.CanUseFlag(sender, flag, area))
				{
					Code code = area.removeFlag(flag);
					switch(code)
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Le flag "+flagName+" a �t� supprim� de l'area "+areaName);
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Failed");
						break;
					}
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+areaName+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Le flag "+flagName+" n'existe pas");
	}
}
