package fr.minecraft.MyWorldGuard.Commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyWorldGuard.Controllers.*;
import fr.minecraft.MyWorldGuard.Models.*;
import fr.minecraft.MyWorldGuard.TabCompleters.AreaTabCompleter;
import fr.minecraft.MyWorldGuard.Utils.AreaSelection;

public class AreaCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "area";
		TAB_COMPLETER = AreaTabCompleter.class;
	}
	
	public static final int MIN_Y=-64, MAX_Y=320;
	private HashMap<UUID, AreaSelection> playersSelection;

	public AreaCommand()
	{
		super();
		this.playersSelection = new HashMap<UUID, AreaSelection>();
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length==0)
		{
			this.inArea(sender);
		}
		else
		{
			switch(args[0])
			{
			case "add":
				this.onAddArg(sender, args);
				break;
			case "remove":
				this.onRemoveArg(sender, args);
				break;
			case "create":
				this.onCreateArg(sender, args);
				break;
			case "recreate":
				this.onRecreateArg(sender, args);
				break;
			case "info":
				this.onInfoArg(sender, args);
				break;
			case "extend":
				this.onExtendArg(sender, args);
				break;
			case "setposition":
				this.onSetPositionArg(sender, args);
				break;
			case "give":
				this.onGiveArg(sender, args);
				break;
			default:
				sender.sendMessage(ChatColor.RED+"Commande invalide");
				break;
			}
		}
		return true;
	}

	private void onCreateArg(CommandSender sender, String[] args)
	{
		if(args.length==2)
		{
			if(sender instanceof Player)
			{
				Player player = (Player) sender;
				AreaController.create(sender, args[1], this.playersSelection.get(player.getUniqueId()));
			}
			else
				sender.sendMessage(ChatColor.RED+"Cette commande ne peut etre utilis� que par un joueur!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
	}

	private void onRecreateArg(CommandSender sender, String[] args) {
		if(args.length==2)
		{
			if(sender instanceof Player)
			{
				Player player = (Player) sender;
				AreaController.recreate(sender, args[1], this.playersSelection.get(player.getUniqueId()));
			}
			else
				sender.sendMessage(ChatColor.RED+"Cette commande ne peut etre utilis� que par un joueur!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
	}

	private void onGiveArg(CommandSender sender, String[] args)
	{
		// area give [area] [player]
		if(args.length == 3)
		{
			AreaController.Give(sender, args[1], args[2]);
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
	}

	private void onSetPositionArg(CommandSender sender, String[] args)
	{
		/*
		 * area setposition <first|second> <x> <y> <z> 
		 * */
		if(args.length==1 || args.length==2 || args.length==5)
		{
			if(sender instanceof Player)
			{
				Player player = (Player) sender;
				Location location=null;
				
				//get location
				if(args.length!=5)
				{
					Block block = player.getTargetBlockExact(AreaSelection.DISTANCE);
					if(block==null)
					{
						sender.sendMessage(ChatColor.RED+"Le bloc cibl� est trop loin!");
						return;
					}
					location = block.getLocation();
				}
				else
				{
					try {
						location = new Location(player.getLocation().getWorld(), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]));
					} catch(Exception e) {
						sender.sendMessage(ChatColor.RED+"Les coordonn�es ne sont pas valide!");
						return;
					}
				}

				//get areaselection
				AreaSelection selection = this.playersSelection.get(player.getUniqueId());

				if(selection == null)
					selection = new AreaSelection();

				//set areaselection
				if((args.length>=2 && args[1].equalsIgnoreCase("first")) || (args.length==1 && selection.first==null))
				{
					selection.first=location;
					sender.sendMessage(ChatColor.GREEN+"Position first d�finie!");
				}
				else if((args.length>=2 && args[1].equalsIgnoreCase("second")) || (args.length==1 && selection.second==null))
				{
					selection.second=location;
					sender.sendMessage(ChatColor.GREEN+"Position second d�finie!");
				}
				else
				{
					sender.sendMessage(ChatColor.RED+"Deux positions sont d�j� d�finie, vous devez pr�ciser laquel vous voulez modifier!");
					return;
				}

				this.playersSelection.put(player.getUniqueId(), selection);

			}
			else
				sender.sendMessage(ChatColor.RED+"Cette commande ne peut etre utilis� que par un joueur");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
	}

	private void onExtendArg(CommandSender sender, String[] args)
	{
		if(args.length==2)
		{
			AreaController.extend(sender, args[1], MIN_Y, MAX_Y);
		}
		else if(args.length==4)
		{
			try {
				int min=Integer.parseInt(args[2]), max=Integer.parseInt(args[3]);
				AreaController.extend(sender, args[1], (min>=MIN_Y&&min<=MAX_Y)?min:MIN_Y, (max>=MIN_Y&&max<=MAX_Y)?max:MAX_Y);
			} catch(Exception e) {
				sender.sendMessage(ChatColor.RED+"Les coordonn�es ne sont pas valide!");
			}
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
	}

	private void onInfoArg(CommandSender sender, String[] args)
	{
		if(args.length==2)
		{
			Area area = Area.GetArea(args[1]);
			if(area!=null)
			{
				area.displayInfo(sender);
			}
			else
				sender.sendMessage(ChatColor.RED+"L'area "+args[1]+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
	}

	private void onRemoveArg(CommandSender sender, String[] args)
	{
		if(args.length==4)
		{
			switch(args[1].toUpperCase())
			{
			case "CHILD":
				ChildController.removeChild(sender, args[2], args[3]);
				break;
			case "FLAG":
				FlagController.removeFlag(sender, args[2], args[3]);
				break;
			case "USER":
				UserController.RemoveUser(sender, args[2], args[3]);
				break;
			case "OWNER":
				OwnerController.removeOwner(sender, args[2], args[3]);
				break;
			default:
				sender.sendMessage(ChatColor.RED+"Commande invalide");
				break;
			}
		}
		else if(args.length==3 && args[1].equalsIgnoreCase("area"))
				AreaController.delete(sender, args[2]);
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
	}

	private void onAddArg(CommandSender sender, String[] args)
	{
		if(args.length==4)
		{
			switch(args[1].toUpperCase())
			{
			case "CHILD":
				ChildController.addChild(sender, args[2], args[3]);
				break;
			case "FLAG":
				FlagController.addFlag(sender, args[2], args[3]);
				break;
			case "USER":
				UserController.AddUser(sender, args[2], args[3]);
				break;
			case "OWNER":
				OwnerController.addOwner(sender, args[2], args[3]);
				break;
			default:
				sender.sendMessage(ChatColor.RED+"Commande invalide");
				break;
			}
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
	}

	/* liste des areas dans lequelles le joueur se trouve */
	public void inArea(CommandSender sender)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			player.sendMessage(ChatColor.DARK_PURPLE+"Vous �tes dans les areas:");
			String cpt = "";
			ArrayList<Area> areas = Area.GetAreas(player.getLocation());

			for(Area i : areas)
			{
				if(!cpt.equals(""))
					cpt = cpt +", ";
				cpt = cpt + i.name;
			}

			if (cpt.equals(""))
				player.sendMessage("Aucune area");
			else
				player.sendMessage(cpt);
		}
		else
			sender.sendMessage(ChatColor.RED+"Vous devez etre un joueur pour utitliser cette commande");
	}
}
