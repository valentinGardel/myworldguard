package fr.minecraft.MyWorldGuard.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import fr.minecraft.MyWorldGuard.Models.Area;
import fr.minecraft.MyWorldGuard.TabCompleters.AreaTabCompleter;

public class AreaOf extends MinecraftCommand
{
	static {
		COMMAND_NAME = "areaOf";
		TAB_COMPLETER = AreaTabCompleter.class;
	}
	
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length == 1)
		{
			String listeArea = "";
			OfflinePlayer player = Bukkit.getOfflinePlayer(args[0]);
			if(player!=null && player.hasPlayedBefore())
			{
				ArrayList<Area> areas = Area.GetAreas(player.getUniqueId());
				for(Area i : areas)
				{
					listeArea += i.name+" ";
				}
				if(listeArea.equals(""))
					sender.sendMessage("Le joueur "+args[0]+" ne poss�de aucune area");
				else {
					sender.sendMessage(ChatColor.GOLD+"Liste des areas du joueur "+args[0]+":");
					sender.sendMessage(listeArea);
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
		return true;
	}
}
