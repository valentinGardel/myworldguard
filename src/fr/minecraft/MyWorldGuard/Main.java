package fr.minecraft.MyWorldGuard;

import Commons.MinecraftPlugin;
import fr.minecraft.MyWorldGuard.Commands.AreaCommand;
import fr.minecraft.MyWorldGuard.Commands.AreaOf;
import fr.minecraft.MyWorldGuard.Commands.AreaSelectorCommand;
import fr.minecraft.MyWorldGuard.Listeners.AreaListener;
import fr.minecraft.MyWorldGuard.Listeners.AreaSelectorListener;
import fr.minecraft.MyWorldGuard.Listeners.NetherPortalTravel;
import fr.minecraft.MyWorldGuard.Models.Area;


@SuppressWarnings("unchecked")
public class Main extends MinecraftPlugin
{
	static {
		PLUGIN_NAME = "MyWorldGuard";
		LOG_TOGGLE = false;
		COMMANDS = new Class[]{
				AreaCommand.class,
				AreaOf.class,
				AreaSelectorCommand.class
			};
		LISTENERS = new Class[]{
				AreaListener.class,
				NetherPortalTravel.class,
				AreaSelectorListener.class
			};
		CONFIG = Config.class;
	}
	
//	public void onEnable()
//	{
//		getServer().getPluginManager().registerEvents(new AreaListener(), this);
//		getServer().getPluginManager().registerEvents(new NetherPortalTravel(), this);
//		
//		this.getCommand("area").setExecutor(new AreaCommand());
//		this.getCommand("area").setTabCompleter(new AreaTabCompleter());
//		
//		this.getCommand("areaof").setExecutor((CommandExecutor) new AreaOf());
//		
//		this.getCommand("/areaSelector").setExecutor(new AreaSelectorCommand());
//		
//		getServer().getPluginManager().registerEvents(new AreaSelectorListener(), this);
//		
//	}

	@Override
	protected void initDatabase() throws Exception
	{
		Area.CreateTables();
	}
}
