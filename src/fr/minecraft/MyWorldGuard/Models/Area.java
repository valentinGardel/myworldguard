package fr.minecraft.MyWorldGuard.Models;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import fr.minecraft.MyWorldGuard.Config;
import fr.minecraft.MyWorldGuard.Utils.Code;
import fr.minecraft.MyWorldGuard.Utils.Flag;

public class Area
{
	private final static String TABLE_AREA="Area",TABLE_USERS="AreaUsers", TABLE_OWNERS="AreaOwners", TABLE_FLAGS="AreaFlags", TABLE_AREA_CHILDS="AreaChilds"; 

	private int id;

	public String name;
	public Location firstLocation, secondLocation;
	public UUID creator;

	private ArrayList<UUID> users, owners;
	private ArrayList<Flag> flags;
	private ArrayList<Integer> childs;
	private ArrayList<Integer> parents;
	
	/**
	 * Public constructor
	 * */
	public Area(String areaName, UUID creator, Location l1, Location l2)
	{
		this.id = -1;
		this.name = areaName;
		this.firstLocation = l1;
		this.secondLocation = l2;
		this.creator = creator;
		this.users= null;//new ArrayList<UUID>();
		this.owners= null;//new ArrayList<UUID>();
		this.flags= null;//new ArrayList<Flag>();
		this.childs = null;//new ArrayList<Integer>();
		this.parents = null;
	}

	/**
	 * Private constructor (adding id setting)
	 * */
	private Area(int id, String areaName, UUID creator, Location l1, Location l2)
	{
		this.id = id;
		this.name = areaName;
		this.firstLocation = l1;
		this.secondLocation = l2;
		this.creator = creator;
		this.users = null;//users;
		this.owners = null;//owners;
		this.flags= null;//flags;
		this.childs = null;//childs;
		this.parents = null;
	}

	/**
	 * create table if necessary
	 * */
	public static void CreateTables() throws SQLException
	{
		//table area
		Statement stmt = Config.GetConfig().getConnection().createStatement();
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS "+TABLE_AREA+"(id int PRIMARY KEY AUTO_INCREMENT, creator varchar(50), name varchar(20) UNIQUE, x1 int, y1 int, z1 int, x2 int, y2 int, z2 int, world varchar(50));");
		//table flags
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS "+TABLE_FLAGS+"(id int, flag varchar(30), primary key (id, flag), foreign key(id) references "+TABLE_AREA+"(id) ON DELETE CASCADE);");
		//table users
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS "+TABLE_USERS+"(id int, player varchar(50), primary key (id, player), foreign key(id) references "+TABLE_AREA+"(id) ON DELETE CASCADE);");
		//table owners
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS "+TABLE_OWNERS+"(id int, player varchar(50), primary key (id, player), foreign key(id) references "+TABLE_AREA+"(id) ON DELETE CASCADE);");
		//table childs
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS "+TABLE_AREA_CHILDS+"(parent int REFERENCES "+TABLE_AREA+"(id) ON DELETE CASCADE, child int REFERENCES "+TABLE_AREA+"(id) ON DELETE CASCADE, PRIMARY KEY (parent, child));");
		stmt.close();
	}

	/**
	 * delete the area in db
	 * */
	public Code delete()
	{
		if(this.id!=-1)
		{
			try {
				Connection con = Config.GetConfig().getConnection();
				PreparedStatement st;

				st = con.prepareStatement("delete from "+TABLE_AREA+" where id=?;");
				st.setInt(1, this.id);
				st.executeUpdate();
				st.close();

				this.id=-1;

			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return null;
			}
		}
		else
			return Code.NotFound;
		return Code.Success;
	}

	/**
	 * saving/updating the area in db (don't handle users/flags/owners/childs)
	 * */
	public Code save()
	{
		//insert into
		if(this.id==-1)
		{
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_AREA+" (name, x1, y1, z1, x2, y2, z2, world, creator) values(?, ?, ?, ?, ?, ?, ?, ?, ?);");
				st.setString(1, this.name);
				st.setInt(2, this.firstLocation.getBlockX());
				st.setInt(3, this.firstLocation.getBlockY());
				st.setInt(4, this.firstLocation.getBlockZ());
				st.setInt(5, this.secondLocation.getBlockX());
				st.setInt(6, this.secondLocation.getBlockY());
				st.setInt(7, this.secondLocation.getBlockZ());
				st.setString(8, this.firstLocation.getWorld().getUID().toString());
				st.setString(9, this.creator.toString());
				st.executeUpdate();
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return Code.Failed;
			}
		}
		//update
		else
		{
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("UPDATE "+TABLE_AREA+" SET name=?, creator=?, x1=?, y1=?, z1=?, x2=?, y2=?, z2=?, world=? where id=?");
				st.setString(1, this.name);
				st.setString(2, this.creator.toString());
				st.setInt(3, this.firstLocation.getBlockX());
				st.setInt(4, this.firstLocation.getBlockY());
				st.setInt(5, this.firstLocation.getBlockZ());
				st.setInt(6, this.secondLocation.getBlockX());
				st.setInt(7, this.secondLocation.getBlockY());
				st.setInt(8, this.secondLocation.getBlockZ());
				st.setString(9, this.firstLocation.getWorld().getUID().toString());
				st.setInt(10, this.id);
				st.executeUpdate();
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return Code.Failed;
			}
		}
		return Code.Success;
	}

	public Code addUser(UUID user)
	{
		if(this.id!=-1)
		{
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_USERS+" (id, player) values(?, ?);");
				st.setInt(1, this.id);
				st.setString(2, user.toString());
				st.executeUpdate();
				this.getUsers().add(user);
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return Code.Failed;
			}
		}
		else
			return Code.NotFound;
		return Code.Success;
	}

	public Code addOwner(UUID owner)
	{
		if(this.id!=-1)
		{
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_OWNERS+" (id, player) values(?, ?);");
				st.setInt(1, this.id);
				st.setString(2, owner.toString());
				st.executeUpdate();
				this.getOwners().add(owner);
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return Code.Failed;
			}
		}
		else
			return Code.NotFound;
		return Code.Success;
	}

	public Code addFlag(Flag flag)
	{
		if(this.id!=-1)
		{
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_FLAGS+" (id, flag) values(?, ?);");
				st.setInt(1, this.id);
				st.setString(2, flag.name());
				st.executeUpdate();
				this.getFlags().add(flag);
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return Code.Failed;
			}
		}
		else
			return Code.NotFound;
		return Code.Success;
	}
	
	public Code addChild(Area area)
	{
		if(this.id!=-1 && area.id!=-1)
		{
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_AREA_CHILDS+" (parent, child) values(?, ?);");
				st.setInt(1, this.id);
				st.setInt(2, area.id);
				st.executeUpdate();
				this.getChilds().add(area.id);
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return Code.Failed;
			}
		}
		else
			return Code.NotFound;
		return Code.Success;
	}

	public Code removeChild(Area area)
	{
		if(this.id!=-1 && area.id!=-1)
		{
			try {
				PreparedStatement st =  Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_AREA_CHILDS+" where parent=? and child=?;");
				st.setInt(1, this.id);
				st.setInt(2, area.id);
				st.executeUpdate();
				st.close();	
				this.getChilds().remove(this.getChilds().indexOf(area.id));
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return null;
			}
		}
		else
			return Code.NotFound;
		return Code.Success;
	}
	
	public Code removeUser(UUID user)
	{
		if(this.id!=-1)
		{
			try {
				PreparedStatement st =  Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_USERS+" where id=? and player=?;");
				st.setInt(1, this.id);
				st.setString(2, user.toString());
				st.executeUpdate();
				st.close();	
				this.getUsers().remove(user);
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return null;
			}
		}
		else
			return Code.NotFound;
		return Code.Success;
	}

	public Code removeOwner(UUID owner)
	{
		if(this.id!=-1)
		{
			try {
				PreparedStatement st =  Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_OWNERS+" where id=? and player=?;");
				st.setInt(1, this.id);
				st.setString(2, owner.toString());
				st.executeUpdate();
				st.close();	
				this.getOwners().remove(owner);
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return null;
			}
		}
		else
			return Code.NotFound;
		return Code.Success;
	}

	public Code removeFlag(Flag flag)
	{
		if(this.id!=-1)
		{
			try {
				PreparedStatement st =  Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_FLAGS+" where id=? and flag=?;");
				st.setInt(1, this.id);
				st.setString(2, flag.name());
				st.executeUpdate();
				st.close();	
				this.getFlags().remove(flag);
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return Code.Failed;
			}
		}
		else
			return Code.NotFound;
		return Code.Success;
	}

	/**
	 * Get Areas from a location
	 * @param location where we want areas
	 * @return list of all areas affecting the location parsed as arg
	 * */
	public static ArrayList<Area> GetAreas(Location location)
	{
		ArrayList<Area> areas = new ArrayList<Area>();

		try {
			Connection con = Config.GetConfig().getConnection();

			PreparedStatement st=con.prepareStatement("Select id, name, creator, x1, x2, y1, y2, z1, z2, world from "+TABLE_AREA+" where ((? between x1 and x2)or(? between x2 and x1)) and ((? between y1 and y2)or(? between y2 and y1)) and ((? between z1 and z2)or(? between z2 and z1)) and world=?;");
			st.setInt(1, location.getBlockX());
			st.setInt(2, location.getBlockX());
			st.setInt(3, location.getBlockY());
			st.setInt(4, location.getBlockY());
			st.setInt(5, location.getBlockZ());
			st.setInt(6, location.getBlockZ());
			st.setString(7, location.getWorld().getUID().toString());

			ResultSet res = st.executeQuery();
			while(res.next())
			{
				World world = Bukkit.getWorld(UUID.fromString(res.getString("world")));
				int id = res.getInt("id");
				areas.add(new Area(
							id,
							res.getString("name"),
							UUID.fromString(res.getString("creator")),
							new Location(world, res.getDouble("x1"), res.getDouble("y1"), res.getDouble("z1")),
							new Location(world, res.getDouble("x2"), res.getDouble("y2"), res.getDouble("z2"))
							)
						);
			}
			res.close();
			st.close();
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}

		return areas ;
	}

	/**
	 * Get Areas from a creator
	 * @param playerUUID of the player who is creator of the areas we return
	 * @return list of areas where the creator is the player parsed as arg
	 * */
	public static ArrayList<Area> GetAreas(UUID playerUUID)
	{
		ArrayList<Area> areas = new ArrayList<Area>();

		try {
			Connection con = Config.GetConfig().getConnection();

			PreparedStatement st=con.prepareStatement("Select id, name, x1, x2, y1, y2, z1, z2, world from "+TABLE_AREA+" where creator=?;");
			st.setString(1, playerUUID.toString());

			ResultSet res = st.executeQuery();
			while(res.next())
			{
				World world = Bukkit.getWorld(UUID.fromString(res.getString("world")));
				int id = res.getInt("id");
				areas.add(new Area(
							id,
							res.getString("name"),
							playerUUID,
							new Location(world, res.getDouble("x1"), res.getDouble("y1"), res.getDouble("z1")),
							new Location(world, res.getDouble("x2"), res.getDouble("y2"), res.getDouble("z2"))
							)
						);
			}
			res.close();
			st.close();
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}

		return areas ;
	}

	/**
	 * Get an Area by his name
	 * @param name of the Area we want
	 * @return Area corresponding to the name
	 * */
	public static Area GetArea(String name)
	{
		Area area=null;

		try {
			Connection con = Config.GetConfig().getConnection();

			PreparedStatement st=con.prepareStatement("Select id, creator, name, x1, x2, y1, y2, z1, z2, world from "+TABLE_AREA+" where name=?;");
			st.setString(1, name);

			ResultSet res = st.executeQuery();
			if(res.next())
			{
				World world = Bukkit.getWorld(UUID.fromString(res.getString("world")));
				int id = res.getInt("id");
				area=new Area(
						id,
						res.getString("name"),
						UUID.fromString(res.getString("creator")),
						new Location(world, res.getDouble("x1"), res.getDouble("y1"), res.getDouble("z1")),
						new Location(world, res.getDouble("x2"), res.getDouble("y2"), res.getDouble("z2"))
						);
			}
			res.close();
			st.close();
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}

		return area;
	}

	private static ArrayList<Integer> GetChilds(int id)
	{
		ArrayList<Integer> childs = new ArrayList<Integer>();
		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select child from "+TABLE_AREA_CHILDS+" where parent=?;");
			st.setInt(1, id);
			ResultSet res = st.executeQuery();
			while(res.next())
			{
				childs.add(res.getInt("child"));
			}
			res.close();
			st.close();
		}catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
		return childs;
	}
	
	private static ArrayList<Integer> GetParents(int id)
	{
		ArrayList<Integer> parents = new ArrayList<Integer>();
		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select parent from "+TABLE_AREA_CHILDS+" where child=?;");
			st.setInt(1, id);
			ResultSet res = st.executeQuery();
			while(res.next())
			{
				parents.add(res.getInt("child"));
			}
			res.close();
			st.close();
		}catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
		return parents;
	}

	/**
	 * Get users of an area by his id
	 * @param id of the area
	 * @return list of UUID of the users
	 * */
	private static ArrayList<UUID> GetUsers(int id)
	{	
		ArrayList<UUID> users = new ArrayList<UUID>();
		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select player from "+TABLE_USERS+" where id=?;");
			st.setInt(1, id);
			ResultSet res = st.executeQuery();
			while(res.next())
			{
				users.add(UUID.fromString(res.getString("player")));
			}
			res.close();
			st.close();
		}catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
		return users;
	}
	
	/**
	 * Get users of an area by his id
	 * @param id of the area
	 * @return list of UUID of the users
	 * */
	private static ArrayList<Flag> GetFlags(int id)
	{	
		ArrayList<Flag> flags = new ArrayList<Flag>();
		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select flag from "+TABLE_FLAGS+" where id=?;");
			st.setInt(1, id);
			ResultSet res = st.executeQuery();
			while(res.next())
			{
				flags.add(Flag.valueOf(res.getString("flag")));
			}
			res.close();
			st.close();
		}catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
		return flags;
	}

	/**
	 * Get owners of an area by his id
	 * @param id of the area
	 * @return list of UUID of the owners
	 * */
	private static ArrayList<UUID> GetOwners(int id)
	{
		ArrayList<UUID> users = new ArrayList<UUID>();
		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select player from "+TABLE_OWNERS+" where id=?;");
			st.setInt(1, id);
			ResultSet res = st.executeQuery();
			while(res.next())
			{
				users.add(UUID.fromString(res.getString("player")));
			}
			res.close();
			st.close();
		}catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
		return users;
	}

	public ArrayList<Flag> getFlags()
	{
		if(this.flags==null)
			this.flags=Area.GetFlags(this.id);
		return this.flags;
	}
	
	public ArrayList<UUID> getUsers()
	{
		if(this.users==null)
			this.users=Area.GetUsers(this.id);
		return this.users;
	}
	
	public ArrayList<UUID> getOwners()
	{
		if(this.owners==null)
			this.owners=Area.GetOwners(this.id);
		return this.owners;
	}
	
	public ArrayList<Integer> getChilds()
	{
		if(this.childs==null)
			this.childs=Area.GetChilds(this.id);
		return this.childs;
	}
	
	public ArrayList<Integer> getParents()
	{
		if(this.parents==null)
			this.parents=Area.GetParents(this.id);
		return this.parents;
	}
	
	/**
	 * Display the informations of the area to the given player
	 * @param sender to which we send info
	 * */
	public void displayInfo(CommandSender sender)
	{
		sender.sendMessage(ChatColor.GOLD+"Information de l'area \""+this.name+"\":");
		sender.sendMessage(ChatColor.GOLD+"Position:"+ChatColor.WHITE+"("+this.firstLocation.getBlockX()+", "+this.firstLocation.getBlockY()+", "+this.firstLocation.getBlockZ()+
				") � ("+this.secondLocation.getBlockX()+", "+this.secondLocation.getBlockY()+", "+this.secondLocation.getBlockZ()+"), monde: "+this.firstLocation.getWorld().getName());
		sender.sendMessage(ChatColor.GOLD+"Createur: "+ChatColor.WHITE+Bukkit.getOfflinePlayer(this.creator).getName());

		String res = "";
		sender.sendMessage(ChatColor.GOLD+"Liste des propri�taires:");
		for(UUID i : this.getOwners()) {
			if(!res.equals(""))
				res = res+", ";
			res = res + Bukkit.getOfflinePlayer(i).getName();	
		}
		if(res.equals(""))
			sender.sendMessage("Aucun propri�taire");
		else
			sender.sendMessage(res);
		sender.sendMessage(ChatColor.GOLD+"Liste des utilisateurs:");
		res = "";
		for(UUID i : this.getUsers()) {
			if(!res.equals(""))
				res = res+", ";
			res = res + Bukkit.getOfflinePlayer(i).getName();
		}
		if(res.equals(""))
			sender.sendMessage("Aucun utilisateur");
		else
			sender.sendMessage(res);
		res = "";
		sender.sendMessage(ChatColor.GOLD+"Liste des flags:");
		for(Flag i : this.getFlags()) {
			if(!res.equals(""))
				res = res+", ";
			res = res + i.name();	
		}
		if(res.equals(""))
			sender.sendMessage("Aucun flag");
		else
			sender.sendMessage(res);
	}
	
	public boolean hasUserRights(UUID player)
	{
		return this.getFlags().contains(Flag.EVERYONE) || this.getUsers().contains(player) || this.hasOwnerRights(player);
	}
	
	public boolean hasOwnerRights(UUID player)
	{
		return this.getOwners().contains(player) || this.hasCreatorRights(player);
	}
	
	public boolean hasCreatorRights(UUID player)
	{
		return this.creator.equals(player);
	}
	
	public boolean equals(Object o)
	{
		return o==this || (o instanceof Area && ((Area)o).id == this.id);
	}
	

	public int getId()
	{
		return this.id;
	}
}
