package fr.minecraft.MyWorldGuard.Utils;

public class Permission {
	public static String 
	BYPASS="MyWorldGuard.bypass",
	FLAG="MyWorldGuard.flag",
	INFINITE_AREA="MyWorldGuard.infiniteArea",
	USE_LAVA="MyWorldGuard.useLava",
	USE_CHILD="MyWorldGuard.child";
}
