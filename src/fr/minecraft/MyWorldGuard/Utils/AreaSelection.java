package fr.minecraft.MyWorldGuard.Utils;

import org.bukkit.Location;

public class AreaSelection
{
	public static int DISTANCE=5;
	public Location first, second;
	
	public AreaSelection() {
		this.first=null;
		this.second=null;
	}
	
	public boolean isValid()
	{
		return this.first != null && this.second != null && this.second.getWorld().getName().equals(this.first.getWorld().getName());
	}

	public int getSize()
	{
		return (Math.abs(this.first.getBlockX() - this.second.getBlockX())+1) * (Math.abs(this.first.getBlockZ() - this.second.getBlockZ())+1);
	}
}
