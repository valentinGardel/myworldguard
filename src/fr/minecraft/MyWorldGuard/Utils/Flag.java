package fr.minecraft.MyWorldGuard.Utils;

public enum Flag
{
	Invincible,
	NoMobs,
	CanOpenChest,
	CanInteract,
	PvP,
	KeepInventory,
	NoEnderPearl,
	NoDeathCount, 
	NoMobsGriefing,
	SHOP,
	EVERYONE
}
